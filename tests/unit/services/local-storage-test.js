import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';

moduleFor('service:local-storage', 'Unit | Service | local storage', {});

test('implements a getter with promises', function(assert) {
  assert.expect(1);
  
  window.localStorage.setItem('testingKey', 'someValue');
  let service = this.subject();

  Ember.run(() => {
    service.get('testingKey').then(value => {
      assert.equal(value, 'someValue');
    });
  });
});

test('implements a setter with promises', function(assert) {
  assert.expect(2);
  let service = this.subject();

  Ember.run(() => {
    service.set('testingKey', 'someNewValue').then(value => {
      assert.equal(value, 'someNewValue');
      assert.equal(window.localStorage.getItem('testingKey'), 'someNewValue');
    });
  });
});
