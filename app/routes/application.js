import Ember from 'ember';

const { inject } = Ember;

export default Ember.Route.extend({
  loadOrRepopulateFromCache: inject.service(),

  model() {
    return this.get('loadOrRepopulateFromCache').findOrRepopulate('user', { per_page: 9999 });
  },
});
