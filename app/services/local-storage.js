import Ember from 'ember';

// Uses promises even though simple writes/reads to local storage
// are synchronous in order to support future implementation of 
// indexdb
export default Ember.Service.extend({
  get(key) {
    return new Ember.RSVP.Promise((resolve) => {
      resolve(window.localStorage.getItem(key));
    });
  },

  set(key, value) {
    return new Ember.RSVP.Promise((resolve) => {
      window.localStorage.setItem(key, value);
      resolve(value);
    });
  }
});
