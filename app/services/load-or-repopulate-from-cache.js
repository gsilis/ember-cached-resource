import Ember from 'ember';

const { inject } = Ember;

export default Ember.Service.extend({
  localStorage: inject.service(),
  store: inject.service(),
  hasCached: undefined,

  init() {
    this.set('hasCached', {});
  },

  findOrRepopulate(modelName, withQuery) {
    const localStorage = this.get('localStorage');
    const cachedDataPromise = localStorage.get(modelName);
    const cachedDataFetchedAtPromise = this._fetchedAtRecently(modelName);

    return Ember.RSVP.allSettled([
      cachedDataPromise,
      cachedDataFetchedAtPromise
    ]).then(([ cachedDataResult, cachedDataFetchedAtResult ]) => {
      const cachedData = cachedDataResult.value;
      const cachedDataFetchedAt = cachedDataFetchedAtResult.value;
      
      if (cachedData && cachedDataFetchedAt) {
        this.set(`hasCached.${modelName}`, true);
        return this._repopulateCachedRecords(modelName, JSON.parse(cachedData));
      } else {
        this._attachHandleResponse(modelName);
        return this.get('store').query(modelName, withQuery);
      }
    });
  },

  _fetchedAtRecently(modelName) {
    const localStorage = this.get('localStorage');
    const fetchedAtPromise = localStorage.get(`${modelName}-fetched-at`);
    let newerThan = new Date();
    newerThan = newerThan.setDate(-30);

    return fetchedAtPromise.then((fetchedAt) => {
      return fetchedAt && parseInt(fetchedAt) > newerThan;
    });
  },

  _repopulateCachedRecords(modelName, cachedUsers) {
    const store = this.get('store');

    store.pushPayload(modelName, cachedUsers);
    return store.peekAll(modelName);
  },

  _attachHandleResponse(modelName) {
    const modelAdapter = this.get('store').adapterFor(modelName);
    let _service = this;

    modelAdapter.reopen({
      handleResponse(status, headers, payload) {
        const hasNotCached = !_service.get(`hasCached.${modelName}`);
        const hasKeyOfInterest = payload[modelName] || payload[modelName.pluralize()];

        if (hasNotCached && hasKeyOfInterest) {
          _service.set(`hasCached.${modelName}`, true);
          const localStorage = _service.get('localStorage');

          localStorage.set(modelName, JSON.stringify(payload));
          localStorage.set(`${modelName}-fetched-at`, (new Date()).valueOf());
        }

        return this._super.apply(this, arguments);
      }
    });
  },
});
